package top.hmtools.wxmp.core.model.message;

import com.thoughtworks.xstream.XStream;

/**
 * 临时的消息实体类
 * @author HyboWork
 *
 */
public class TempMessage extends BaseEventMessage {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7723518584794077290L;

	@Override
	public void configXStream(XStream xStream) {
		xStream.ignoreUnknownElements();
	}

}
