### 本工具包是对 http client 4.5.6 版本的部分功能包装，用于简化在应用中的使用。
1. 支持HTTPS
2. 支持添加http代理
3. 支持基于连接池、路由
4. 支持设置重试机制

### 说明
1. `top.hmtools.wxmp.core.httpclient.HmHttpClientFactoryHandle`主要是封装对httpclient的设置以及对象实例的获取。
2. `top.hmtools.wxmp.core.httpclient.HmHttpClientTools`主要是对http request请求的封装，简化使用，同时进行了对路由、http request context的自动处理。 

### 示例：
- `top.hmtools.httpclient.hm2020test.HmHttpClientToolsTest`示例位置

1. 快速示例：
```
	/**
	 * 1. 基础快速测试示例
	 * http get 请求百度首页，获取 HTML 字符串并打印
	 */
	@Test
	public void fastTestAAA(){
		String result = HmHttpClientTools.httpGetReqParamRespString("https://www.baidu.com", null);
		System.out.println(result);
	}
```
输出的日志打印结果是：
```
2020-04-24 13:28:18,402  [main] DEBUG top.hmtools.wxmp.core.httpclient.HmHttpClientTools.httpGetReqParamRespString() - 向服务侧发起http get请求的完整URL是：https://www.baidu.com
2020-04-24 13:28:19,721  [main] DEBUG top.hmtools.wxmp.core.httpclient.HmHttpClientFactoryHandle.fillConfig() - 当前设置的代理是：null
2020-04-24 13:28:19,722  [main] DEBUG top.hmtools.wxmp.core.httpclient.HmHttpClientFactoryHandle.fillConfig() - 当前设置的request config 是：null
2020-04-24 13:28:19,724  [main] DEBUG top.hmtools.wxmp.core.httpclient.HmHttpClientFactoryHandle.fillConfig() - 当前设置的重试机制 是：null
2020-04-24 13:28:19,724  [main] DEBUG top.hmtools.wxmp.core.httpclient.HmHttpClientFactoryHandle.fillConfig() - 当前设置的connection keep alive strategy 是：null

（此处省略若干行）

2020-04-24 13:28:20,333  [main] DEBUG top.hmtools.wxmp.core.httpclient.responseHandler.ResponseHandlerString.handleResponse() - http请求获取的响应状态码是：200，原文内容是：<!DOCTYPE html>
<!--STATUS OK--><html> <head><meta http-equiv=content-type content=text/html;charset=utf-8><meta http-equiv=X-UA-Compatible content=IE=Edge><meta content=always name=referrer><link rel=stylesheet type=text/css href=https://ss1.bdstatic.com/5eN1bjq8AAUYm2zgoY3K/r/www/cache/bdorz/baidu.min.css><title>百度一下，你就知道</title></head> <body link=#0000cc> <div id=wrapper> <div id=head> <div class=head_wrapper> <div class=s_form> <div class=s_form_wrapper> <div id=lg> <img hidefocus=true src=//www.baidu.com/img/bd_logo1.png width=270 height=129> </div> <form id=form name=f action=//www.baidu.com/s class=fm> <input type=hidden name=bdorz_come value=1> <input type=hidden name=ie value=utf-8> <input type=hidden name=f value=8> <input type=hidden name=rsv_bp value=1> <input type=hidden name=rsv_idx value=1> <input type=hidden name=tn value=baidu><span class="bg s_ipt_wr"><input id=kw name=wd class=s_ipt value maxlength=255 autocomplete=off autofocus=autofocus></span><span class="bg s_btn_wr"><input type=submit id=su value=百度一下 class="bg s_btn" autofocus></span> </form> </div> </div> <div id=u1> <a href=http://news.baidu.com name=tj_trnews class=mnav>新闻</a> <a href=https://www.hao123.com name=tj_trhao123 class=mnav>hao123</a> <a href=http://map.baidu.com name=tj_trmap class=mnav>地图</a> <a href=http://v.baidu.com name=tj_trvideo class=mnav>视频</a> <a href=http://tieba.baidu.com name=tj_trtieba class=mnav>贴吧</a> <noscript> <a href=http://www.baidu.com/bdorz/login.gif?login&amp;tpl=mn&amp;u=http%3A%2F%2Fwww.baidu.com%2f%3fbdorz_come%3d1 name=tj_login class=lb>登录</a> </noscript> <script>document.write('<a href="http://www.baidu.com/bdorz/login.gif?login&tpl=mn&u='+ encodeURIComponent(window.location.href+ (window.location.search === "" ? "?" : "&")+ "bdorz_come=1")+ '" name="tj_login" class="lb">登录</a>');
                </script> <a href=//www.baidu.com/more/ name=tj_briicon class=bri style="display: block;">更多产品</a> </div> </div> </div> <div id=ftCon> <div id=ftConw> <p id=lh> <a href=http://home.baidu.com>关于百度</a> <a href=http://ir.baidu.com>About Baidu</a> </p> <p id=cp>&copy;2017&nbsp;Baidu&nbsp;<a href=http://www.baidu.com/duty/>使用百度前必读</a>&nbsp; <a href=http://jianyi.baidu.com/ class=cp-feedback>意见反馈</a>&nbsp;京ICP证030173号&nbsp; <img src=//www.baidu.com/img/gs.gif> </p> </div> </div> </div> </body> </html>

2020-04-24 13:28:20,336  [main] DEBUG org.apache.http.impl.conn.PoolingHttpClientConnectionManager.releaseConnection() - Connection [id: 0][route: {s}->https://www.baidu.com:443] can be kept alive indefinitely
2020-04-24 13:28:20,337  [main] DEBUG org.apache.http.impl.conn.DefaultManagedHttpClientConnection.setSocketTimeout() - http-outgoing-0: set socket timeout to 0
2020-04-24 13:28:20,337  [main] DEBUG org.apache.http.impl.conn.PoolingHttpClientConnectionManager.releaseConnection() - Connection released: [id: 0][route: {s}->https://www.baidu.com:443][total kept alive: 1; route allocated: 1 of 20; total allocated: 1 of 200]
<!DOCTYPE html>
<!--STATUS OK--><html> <head><meta http-equiv=content-type content=text/html;charset=utf-8><meta http-equiv=X-UA-Compatible content=IE=Edge><meta content=always name=referrer><link rel=stylesheet type=text/css href=https://ss1.bdstatic.com/5eN1bjq8AAUYm2zgoY3K/r/www/cache/bdorz/baidu.min.css><title>百度一下，你就知道</title></head> <body link=#0000cc> <div id=wrapper> <div id=head> <div class=head_wrapper> <div class=s_form> <div class=s_form_wrapper> <div id=lg> <img hidefocus=true src=//www.baidu.com/img/bd_logo1.png width=270 height=129> </div> <form id=form name=f action=//www.baidu.com/s class=fm> <input type=hidden name=bdorz_come value=1> <input type=hidden name=ie value=utf-8> <input type=hidden name=f value=8> <input type=hidden name=rsv_bp value=1> <input type=hidden name=rsv_idx value=1> <input type=hidden name=tn value=baidu><span class="bg s_ipt_wr"><input id=kw name=wd class=s_ipt value maxlength=255 autocomplete=off autofocus=autofocus></span><span class="bg s_btn_wr"><input type=submit id=su value=百度一下 class="bg s_btn" autofocus></span> </form> </div> </div> <div id=u1> <a href=http://news.baidu.com name=tj_trnews class=mnav>新闻</a> <a href=https://www.hao123.com name=tj_trhao123 class=mnav>hao123</a> <a href=http://map.baidu.com name=tj_trmap class=mnav>地图</a> <a href=http://v.baidu.com name=tj_trvideo class=mnav>视频</a> <a href=http://tieba.baidu.com name=tj_trtieba class=mnav>贴吧</a> <noscript> <a href=http://www.baidu.com/bdorz/login.gif?login&amp;tpl=mn&amp;u=http%3A%2F%2Fwww.baidu.com%2f%3fbdorz_come%3d1 name=tj_login class=lb>登录</a> </noscript> <script>document.write('<a href="http://www.baidu.com/bdorz/login.gif?login&tpl=mn&u='+ encodeURIComponent(window.location.href+ (window.location.search === "" ? "?" : "&")+ "bdorz_come=1")+ '" name="tj_login" class="lb">登录</a>');
                </script> <a href=//www.baidu.com/more/ name=tj_briicon class=bri style="display: block;">更多产品</a> </div> </div> </div> <div id=ftCon> <div id=ftConw> <p id=lh> <a href=http://home.baidu.com>关于百度</a> <a href=http://ir.baidu.com>About Baidu</a> </p> <p id=cp>&copy;2017&nbsp;Baidu&nbsp;<a href=http://www.baidu.com/duty/>使用百度前必读</a>&nbsp; <a href=http://jianyi.baidu.com/ class=cp-feedback>意见反馈</a>&nbsp;京ICP证030173号&nbsp; <img src=//www.baidu.com/img/gs.gif> </p> </div> </div> </div> </body> </html>

```

2. 带有请求参数的快速示例：
```
	/**
	 * 2. 基础快速测试示例
	 * http get 请求百度首页，获取 HTML 字符串并打印
	 * 带上一些请求参数
	 */
	@Test
	public void fastTestBBB(){
		Map<String,Object> params = new HashMap<>();
		params.put("aa", "aaaaaa");
		params.put("bb", 33);
		params.put("cc", 2.2);
		params.put("dd", true);
		
		String result = HmHttpClientTools.httpGetReqParamRespString("https://www.baidu.com", params);
		System.out.println(result);
	}
```

输出的日志打印结果是：
```
2020-04-24 13:32:31,742  [main] DEBUG top.hmtools.wxmp.core.httpclient.HmHttpClientTools.httpGetReqParamRespString() - 向服务侧发起http get请求的完整URL是：https://www.baidu.com?aa=aaaaaa&bb=33&cc=2.2&dd=true
2020-04-24 13:32:32,213  [main] DEBUG top.hmtools.wxmp.core.httpclient.HmHttpClientFactoryHandle.fillConfig() - 当前设置的代理是：null
2020-04-24 13:32:32,213  [main] DEBUG top.hmtools.wxmp.core.httpclient.HmHttpClientFactoryHandle.fillConfig() - 当前设置的request config 是：null
2020-04-24 13:32:32,213  [main] DEBUG top.hmtools.wxmp.core.httpclient.HmHttpClientFactoryHandle.fillConfig() - 当前设置的重试机制 是：null
2020-04-24 13:32:32,214  [main] DEBUG top.hmtools.wxmp.core.httpclient.HmHttpClientFactoryHandle.fillConfig() - 当前设置的connection keep alive strategy 是：null
2020-04-24 13:32:32,237  [main] DEBUG org.apache.http.client.protocol.RequestAddCookies.process() - CookieSpec selected: default

（此处省略若干行）

2020-04-24 13:32:32,451  [main] DEBUG org.apache.http.impl.execchain.MainClientExec.execute() - Connection can be kept alive indefinitely
2020-04-24 13:32:32,457  [main] DEBUG top.hmtools.wxmp.core.httpclient.responseHandler.ResponseHandlerString.handleResponse() - http请求获取的响应状态码是：200，原文内容是：<!DOCTYPE html>
<!--STATUS OK--><html> <head><meta http-equiv=content-type content=text/html;charset=utf-8><meta http-equiv=X-UA-Compatible content=IE=Edge><meta content=always name=referrer><link rel=stylesheet type=text/css href=https://ss1.bdstatic.com/5eN1bjq8AAUYm2zgoY3K/r/www/cache/bdorz/baidu.min.css><title>百度一下，你就知道</title></head> <body link=#0000cc> <div id=wrapper> <div id=head> <div class=head_wrapper> <div class=s_form> <div class=s_form_wrapper> <div id=lg> <img hidefocus=true src=//www.baidu.com/img/bd_logo1.png width=270 height=129> </div> <form id=form name=f action=//www.baidu.com/s class=fm> <input type=hidden name=bdorz_come value=1> <input type=hidden name=ie value=utf-8> <input type=hidden name=f value=8> <input type=hidden name=rsv_bp value=1> <input type=hidden name=rsv_idx value=1> <input type=hidden name=tn value=baidu><span class="bg s_ipt_wr"><input id=kw name=wd class=s_ipt value maxlength=255 autocomplete=off autofocus=autofocus></span><span class="bg s_btn_wr"><input type=submit id=su value=百度一下 class="bg s_btn" autofocus></span> </form> </div> </div> <div id=u1> <a href=http://news.baidu.com name=tj_trnews class=mnav>新闻</a> <a href=https://www.hao123.com name=tj_trhao123 class=mnav>hao123</a> <a href=http://map.baidu.com name=tj_trmap class=mnav>地图</a> <a href=http://v.baidu.com name=tj_trvideo class=mnav>视频</a> <a href=http://tieba.baidu.com name=tj_trtieba class=mnav>贴吧</a> <noscript> <a href=http://www.baidu.com/bdorz/login.gif?login&amp;tpl=mn&amp;u=http%3A%2F%2Fwww.baidu.com%2f%3fbdorz_come%3d1 name=tj_login class=lb>登录</a> </noscript> <script>document.write('<a href="http://www.baidu.com/bdorz/login.gif?login&tpl=mn&u='+ encodeURIComponent(window.location.href+ (window.location.search === "" ? "?" : "&")+ "bdorz_come=1")+ '" name="tj_login" class="lb">登录</a>');
                </script> <a href=//www.baidu.com/more/ name=tj_briicon class=bri style="display: block;">更多产品</a> </div> </div> </div> <div id=ftCon> <div id=ftConw> <p id=lh> <a href=http://home.baidu.com>关于百度</a> <a href=http://ir.baidu.com>About Baidu</a> </p> <p id=cp>&copy;2017&nbsp;Baidu&nbsp;<a href=http://www.baidu.com/duty/>使用百度前必读</a>&nbsp; <a href=http://jianyi.baidu.com/ class=cp-feedback>意见反馈</a>&nbsp;京ICP证030173号&nbsp; <img src=//www.baidu.com/img/gs.gif> </p> </div> </div> </div> </body> </html>

2020-04-24 13:32:32,458  [main] DEBUG org.apache.http.impl.conn.PoolingHttpClientConnectionManager.releaseConnection() - Connection [id: 0][route: {s}->https://www.baidu.com:443] can be kept alive indefinitely
2020-04-24 13:32:32,458  [main] DEBUG org.apache.http.impl.conn.DefaultManagedHttpClientConnection.setSocketTimeout() - http-outgoing-0: set socket timeout to 0
2020-04-24 13:32:32,458  [main] DEBUG org.apache.http.impl.conn.PoolingHttpClientConnectionManager.releaseConnection() - Connection released: [id: 0][route: {s}->https://www.baidu.com:443][total kept alive: 1; route allocated: 1 of 20; total allocated: 1 of 200]
<!DOCTYPE html>
<!--STATUS OK--><html> <head><meta http-equiv=content-type content=text/html;charset=utf-8><meta http-equiv=X-UA-Compatible content=IE=Edge><meta content=always name=referrer><link rel=stylesheet type=text/css href=https://ss1.bdstatic.com/5eN1bjq8AAUYm2zgoY3K/r/www/cache/bdorz/baidu.min.css><title>百度一下，你就知道</title></head> <body link=#0000cc> <div id=wrapper> <div id=head> <div class=head_wrapper> <div class=s_form> <div class=s_form_wrapper> <div id=lg> <img hidefocus=true src=//www.baidu.com/img/bd_logo1.png width=270 height=129> </div> <form id=form name=f action=//www.baidu.com/s class=fm> <input type=hidden name=bdorz_come value=1> <input type=hidden name=ie value=utf-8> <input type=hidden name=f value=8> <input type=hidden name=rsv_bp value=1> <input type=hidden name=rsv_idx value=1> <input type=hidden name=tn value=baidu><span class="bg s_ipt_wr"><input id=kw name=wd class=s_ipt value maxlength=255 autocomplete=off autofocus=autofocus></span><span class="bg s_btn_wr"><input type=submit id=su value=百度一下 class="bg s_btn" autofocus></span> </form> </div> </div> <div id=u1> <a href=http://news.baidu.com name=tj_trnews class=mnav>新闻</a> <a href=https://www.hao123.com name=tj_trhao123 class=mnav>hao123</a> <a href=http://map.baidu.com name=tj_trmap class=mnav>地图</a> <a href=http://v.baidu.com name=tj_trvideo class=mnav>视频</a> <a href=http://tieba.baidu.com name=tj_trtieba class=mnav>贴吧</a> <noscript> <a href=http://www.baidu.com/bdorz/login.gif?login&amp;tpl=mn&amp;u=http%3A%2F%2Fwww.baidu.com%2f%3fbdorz_come%3d1 name=tj_login class=lb>登录</a> </noscript> <script>document.write('<a href="http://www.baidu.com/bdorz/login.gif?login&tpl=mn&u='+ encodeURIComponent(window.location.href+ (window.location.search === "" ? "?" : "&")+ "bdorz_come=1")+ '" name="tj_login" class="lb">登录</a>');
                </script> <a href=//www.baidu.com/more/ name=tj_briicon class=bri style="display: block;">更多产品</a> </div> </div> </div> <div id=ftCon> <div id=ftConw> <p id=lh> <a href=http://home.baidu.com>关于百度</a> <a href=http://ir.baidu.com>About Baidu</a> </p> <p id=cp>&copy;2017&nbsp;Baidu&nbsp;<a href=http://www.baidu.com/duty/>使用百度前必读</a>&nbsp; <a href=http://jianyi.baidu.com/ class=cp-feedback>意见反馈</a>&nbsp;京ICP证030173号&nbsp; <img src=//www.baidu.com/img/gs.gif> </p> </div> </div> </div> </body> </html>


```

3. 增加一些配置的快速示例：
```
	/**
	 * 3. 基础快速测试示例
	 * http get 请求百度首页，获取 HTML 字符串并打印
	 * 增加一些配置
	 */
	@Test
	public void fastTestCCC(){
		//设置连接池的连接数
		HmHttpClientFactoryHandle.setPoolingHttpClientConnectionManager(1, 1);
		//设置长连接？？
		HmHttpClientFactoryHandle.setConnectionKeepAliveStrategy(new ConnectionKeepAliveStrategyDemoImpl());
		//设置超时
		HmHttpClientFactoryHandle.setRequestConfigBuilder(1000, 1000, 1000);
		//设置重试方案
		HmHttpClientFactoryHandle.setHttpRequestRetryHandler(new HttpRequestRetryHandlerDemoImpl());
		//添加http代理（随便在网上找了个免费的代理）
		HmHttpClientFactoryHandle.addProxies(new HttpHost("14.20.235.73", 808,"http"));
		
		String result = HmHttpClientTools.httpGetReqParamRespString("https://www.baidu.com", null);
		System.out.println(result);
	}
```

输出的日志打印结果是：

```
2020-04-24 13:36:28,023  [main] DEBUG top.hmtools.wxmp.core.httpclient.HmHttpClientTools.httpGetReqParamRespString() - 向服务侧发起http get请求的完整URL是：https://www.baidu.com
2020-04-24 13:36:28,044  [main] DEBUG top.hmtools.wxmp.core.httpclient.HmHttpClientFactoryHandle.fillConfig() - 当前设置的代理是：[http://14.20.235.73:808]
2020-04-24 13:36:28,044  [main] DEBUG top.hmtools.wxmp.core.httpclient.HmHttpClientFactoryHandle.fillConfig() - 当前设置的request config 是：[expectContinueEnabled=false, proxy=null, localAddress=null, cookieSpec=null, redirectsEnabled=true, relativeRedirectsAllowed=true, maxRedirects=50, circularRedirectsAllowed=false, authenticationEnabled=true, targetPreferredAuthSchemes=null, proxyPreferredAuthSchemes=null, connectionRequestTimeout=1000, connectTimeout=1000, socketTimeout=1000, contentCompressionEnabled=true]
2020-04-24 13:36:28,045  [main] DEBUG top.hmtools.wxmp.core.httpclient.HmHttpClientFactoryHandle.fillConfig() - 当前设置的重试机制 是：top.hmtools.wxmp.core.httpclient.impl.HttpRequestRetryHandlerDemoImpl@49ec71f8
2020-04-24 13:36:28,046  [main] DEBUG top.hmtools.wxmp.core.httpclient.HmHttpClientFactoryHandle.fillConfig() - 当前设置的connection keep alive strategy 是：top.hmtools.wxmp.core.httpclient.impl.ConnectionKeepAliveStrategyDemoImpl@1d2adfbe
2020-04-24 13:36:28,101  [main] DEBUG org.apache.http.client.protocol.RequestAddCookies.process() - CookieSpec selected: default

（此处省略若干行）


```












































