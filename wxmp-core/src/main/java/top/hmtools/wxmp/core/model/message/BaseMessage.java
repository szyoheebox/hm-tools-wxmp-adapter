package top.hmtools.wxmp.core.model.message;

import java.io.Serializable;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeansException;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.annotations.XStreamAlias;

import top.hmtools.wxmp.core.model.message.enums.MsgType;
import top.hmtools.wxmp.core.tools.XStreamTools;

/**
 * 基础的消息数据实体类，这些消息均是被动接收来自微信服务器的通知类型的xml数据结构数据
 * 
 * @author HyboWork
 *
 */
public abstract class BaseMessage implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3336594454553059603L;
	/**
	 * 接收方微信号
	 */
	@XStreamAlias("ToUserName")
	protected String toUserName;
	/**
	 * 发送方微信号，若为普通用户，则是一个OpenID
	 */
	@XStreamAlias("FromUserName")
	protected String fromUserName;
	/**
	 * 消息创建时间
	 */
	@XStreamAlias("CreateTime")
	protected long createTime;
	/**
	 * 消息类型
	 */
	@XStreamAlias("MsgType")
	protected MsgType msgType;
	/**
	 * 消息id，64位整型
	 */
	@XStreamAlias("MsgId")
	protected String msgId;

	public String getToUserName() {
		return toUserName;
	}

	public void setToUserName(String toUserName) {
		this.toUserName = toUserName;
	}

	public String getFromUserName() {
		return fromUserName;
	}

	public void setFromUserName(String fromUserName) {
		this.fromUserName = fromUserName;
	}

	public long getCreateTime() {
		return createTime;
	}

	public void setCreateTime(long createTime) {
		this.createTime = createTime;
	}

	public void setMsgType(String msgType) {
		if (msgType == null || msgType.length() < 1) {
			return;
		}

		MsgType[] values = MsgType.values();
		for (MsgType ee : values) {
			if (ee.getName().equals(msgType)) {
				this.msgType = ee;
			}
		}
	}

	public MsgType getMsgType() {
		return msgType;
	}

	public void setMsgType(MsgType msgType) {
		this.msgType = msgType;
	}

	public String getMsgId() {
		return msgId;
	}

	public void setMsgId(String msgId) {
		this.msgId = msgId;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * 将本对象实例转换成 微信公众号 接口文档所要求的XML格式字符串
	 * 
	 * @return
	 */
	public String toXmlMsg() {
		return this.getXStream().toXML(this);
	}

	/**
	 * 获取 xstream
	 * 
	 * @return
	 */
	public XStream getXStream() {
		XStream xStream = XStreamTools.initXStream();
		xStream.alias("xml", this.getClass());//设置根标签对应具体的类的class
		this.configXStream(xStream);
		xStream.processAnnotations(this.getClass());// 支持使用注解
		return xStream;
	}

	/**
	 * 必要时，对XStream进行一些设置操作
	 * 
	 * @param xStream
	 */
	public abstract void configXStream(XStream xStream);

	/**
	 * 从XML字符串中读取并解析数据到本实体类对象实例
	 * 
	 * @param xmlMsg
	 */
	public void loadDataFromXml(String xmlMsg) {
		if (xmlMsg == null || xmlMsg.length() < 1) {
			return;
		}

		Object fromXML = this.getXStream().fromXML(xmlMsg);
		try {
			BeanUtils.copyProperties(fromXML, this);
		} catch (Exception e) {
			throw new IllegalArgumentException("从XML中读取数据到本实体类失败", e);
		}
	}
}
