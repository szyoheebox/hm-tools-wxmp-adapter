package top.hmtools.dynamicProxy;

import java.util.Map;

/**
 * 模拟mybatis的mapper接口
 * @author HyboWork
 *
 */
public interface IAaaMapper {

	public Map doAaa(String aa,Integer bb,Map<String,String> map);
	
	public void doAbb();
	
	public void doAcc();
}
