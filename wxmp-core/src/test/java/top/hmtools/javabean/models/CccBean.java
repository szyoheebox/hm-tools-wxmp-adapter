package top.hmtools.javabean.models;

public class CccBean {

	private String cccName;
	
	private int cccAge;

	public String getCccName() {
		return cccName;
	}

	public void setCccName(String cccName) {
		this.cccName = cccName;
	}

	public int getCccAge() {
		return cccAge;
	}

	public void setCccAge(int cccAge) {
		this.cccAge = cccAge;
	}

	@Override
	public String toString() {
		return "CccBean [cccName=" + cccName + ", cccAge=" + cccAge + "]";
	}
	
	
}
