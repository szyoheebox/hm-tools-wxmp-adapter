package top.hmtools.wxmp.material.apis;

import java.io.InputStream;

import top.hmtools.wxmp.core.annotation.WxmpApi;
import top.hmtools.wxmp.core.annotation.WxmpMapper;
import top.hmtools.wxmp.core.enums.HttpMethods;
import top.hmtools.wxmp.core.enums.HttpParamType;
import top.hmtools.wxmp.material.model.MediaBean;
import top.hmtools.wxmp.material.model.MediaParam;
import top.hmtools.wxmp.material.model.UploadParam;
import top.hmtools.wxmp.material.model.VideoResult;

@WxmpMapper
public interface ITemporaryApi {

	/**
	 * 上传临时素材
	 * @param uploadParam
	 * @return
	 */
	@WxmpApi(httpMethods=HttpMethods.POST,uri = "/cgi-bin/media/upload",httpParamType=HttpParamType.FORM_DATA)
	public MediaBean uploadMedia(UploadParam uploadParam);
	
	/**
	 * 获取图片临时素材
	 * @param mediaParam
	 * @return
	 */
	@WxmpApi(httpMethods=HttpMethods.GET,uri="/cgi-bin/media/get")
	public InputStream getImage(MediaParam mediaParam);
	
	/**
	 * 获取视频临时素材
	 * @param mediaParam
	 * @return
	 */
	@WxmpApi(httpMethods=HttpMethods.GET,uri="/cgi-bin/media/get")
	public VideoResult getVideo(MediaParam mediaParam);
	
	/**
	 * 获取从JSSDK的uploadVoice接口上传的临时语音素材，格式为speex，16K采样率。该音频比上文的临时素材获取接口（格式为amr，8K采样率）更加清晰，适合用作语音识别等对音质要求较高的业务。
	 * @param mediaParam
	 * @return
	 */
	@WxmpApi(httpMethods=HttpMethods.GET,uri="/cgi-bin/media/get/jssdk")
	public InputStream getVoice(MediaParam mediaParam);
}
