package top.hmtools.wxmp.user.enums;

import top.hmtools.wxmp.user.model.eventMessage.LocationMessage;

/**
 * 自定义菜单事件实体类枚举集合
 * @author HyboWork
 *
 */
public enum EUserEventMessages {
	
	Location_Message("获取用户地理位置",LocationMessage.class)
	;

	private String eventName;
	
	private Class<?> className;
	
	private String remark;
	
	private EUserEventMessages(String eName,Class<?> clazz) {
		this.eventName = eName;
		this.className = clazz;
	}

	public String getEventName() {
		return eventName;
	}

	public Class<?> getClassName() {
		return className;
	}

	public String getRemark() {
		return remark;
	}

	
	
}
