package top.hmtools.wxmp.user.apis;

import top.hmtools.wxmp.core.annotation.WxmpApi;
import top.hmtools.wxmp.core.annotation.WxmpMapper;
import top.hmtools.wxmp.core.enums.HttpMethods;
import top.hmtools.wxmp.core.model.ErrcodeBean;
import top.hmtools.wxmp.user.model.BatchTagParam;
import top.hmtools.wxmp.user.model.TagFunsParam;
import top.hmtools.wxmp.user.model.TagFunsResult;
import top.hmtools.wxmp.user.model.TagListParam;
import top.hmtools.wxmp.user.model.TagListResult;
import top.hmtools.wxmp.user.model.TagWapperParam;
import top.hmtools.wxmp.user.model.TagWapperResult;
import top.hmtools.wxmp.user.model.TagsWapperResult;

@WxmpMapper
public interface ITagsApi {

	/**
	 * 创建标签
	 * <br>一个公众号，最多可以创建100个标签。
	 * @param param
	 * @return
	 */
	@WxmpApi(httpMethods=HttpMethods.POST,uri="/cgi-bin/tags/create")
	public TagWapperResult create(TagWapperParam param);
	
	/**
	 * 2. 获取公众号已创建的标签
	 */
	@WxmpApi(httpMethods=HttpMethods.GET,uri="/cgi-bin/tags/get")
	public TagsWapperResult get();
	
	/**
	 * 3. 编辑标签
	 * @param param
	 * @return
	 */
	@WxmpApi(httpMethods=HttpMethods.POST,uri="/cgi-bin/tags/update")
	public ErrcodeBean update(TagWapperParam param);
	
	/**
	 * 4. 删除标签
	 * @param param
	 * @return
	 */
	@WxmpApi(httpMethods=HttpMethods.POST,uri="/cgi-bin/tags/delete")
	public ErrcodeBean delete(TagWapperParam param);
	
	/**
	 * 5. 获取标签下粉丝列表
	 * @param param
	 * @return
	 */
	@WxmpApi(httpMethods=HttpMethods.POST,uri="/cgi-bin/user/tag/get")
	public TagFunsResult getFunsOfTag(TagFunsParam param);
	
	/**
	 * 1. 批量为用户打标签
	 * @param batchTagParam
	 * @return
	 */
	@WxmpApi(httpMethods=HttpMethods.POST,uri="/cgi-bin/tags/members/batchtagging")
	public ErrcodeBean batchTagging(BatchTagParam batchTagParam);
	
	/**
	 * 2. 批量为用户取消标签
	 * @param batchTagParam
	 * @return
	 */
	@WxmpApi(httpMethods=HttpMethods.POST,uri="/cgi-bin/tags/members/batchuntagging")
	public ErrcodeBean batchUntagging(BatchTagParam batchTagParam);
	
	/**
	 * 3. 获取用户身上的标签列表
	 * @param param
	 * @return
	 */
	@WxmpApi(httpMethods=HttpMethods.POST,uri="/cgi-bin/tags/getidlist")
	public TagListResult getTaglistByOpenid(TagListParam param);
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
