package top.hmtools.wxmp.message.group.model.preview;

/**
 * Auto-generated: 2019-08-26 10:27:27
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class Text {

	private String content;

	public void setContent(String content) {
		this.content = content;
	}

	public String getContent() {
		return content;
	}

}