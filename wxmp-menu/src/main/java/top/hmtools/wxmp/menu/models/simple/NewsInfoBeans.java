package top.hmtools.wxmp.menu.models.simple;

import java.util.List;

/**
 * 图文消息的信息
 * @author Hybomyth
 *
 */
public class NewsInfoBeans {

	/**
	 * 图文消息的信息集合
	 */
	private List<NewsInfoBean> list;

	/**
	 * 图文消息的信息集合
	 * @return
	 */
	public List<NewsInfoBean> getList() {
		return list;
	}

	/**
	 * 图文消息的信息集合
	 * @param list
	 */
	public void setList(List<NewsInfoBean> list) {
		this.list = list;
	}
	
	
}
