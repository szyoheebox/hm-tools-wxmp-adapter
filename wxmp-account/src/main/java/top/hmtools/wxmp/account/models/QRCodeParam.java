package top.hmtools.wxmp.account.models;

import top.hmtools.wxmp.account.enums.EQRActionName;

/**
 * 微信公众号号接口之 账号管理 之 生成带参数的二维码 接口 的请求参数实体类
 * @author Hybomyth
 *
 */
public class QRCodeParam {

	/**
	 * 该二维码有效时间，以秒为单位。 最大不超过2592000（即30天），此字段如果不填，则默认有效期为30秒。
	 */
	private Long expire_seconds;
	
	/**
	 * 二维码类型，QR_SCENE为临时的整型参数值，QR_STR_SCENE为临时的字符串参数值，QR_LIMIT_SCENE为永久的整型参数值，QR_LIMIT_STR_SCENE为永久的字符串参数值
	 */
	private EQRActionName action_name;
	
	/**
	 * 二维码详细信息
	 */
	private ActionInfo action_info;
	
	public Long getExpire_seconds() {
		return expire_seconds;
	}

	public void setExpire_seconds(Long expire_seconds) {
		this.expire_seconds = expire_seconds;
	}

	public EQRActionName getAction_name() {
		return action_name;
	}

	public void setAction_name(EQRActionName action_name) {
		this.action_name = action_name;
	}

	public ActionInfo getAction_info() {
		return action_info;
	}

	public void setAction_info(ActionInfo action_info) {
		this.action_info = action_info;
	}

	@Override
	public String toString() {
		return "QRCodeParamBean [expire_seconds=" + expire_seconds + ", action_name=" + action_name + ", action_info="
				+ action_info + "]";
	}

	
}
